package com.banquito.accounting.service;

import com.banquito.accounting.dao.GeneralLedgerAccountRepository;
import com.banquito.accounting.dao.JournalEntryRepository;
import com.banquito.accounting.dto.TransactionDTO;
import com.banquito.accounting.enums.LedgerAccountTypeEnum;
import com.banquito.accounting.model.GeneralLedgerAccount;
import com.banquito.accounting.model.JournalEntry;
import com.banquito.accounting.model.JournalEntryDetail;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.assertj.core.api.Assertions.assertThat;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@ExtendWith(MockitoExtension.class)
class JournalEntryServiceTest {

    @Mock
    private JournalEntryRepository journalEntryRepository;
    @Mock
    private GeneralLedgerAccountRepository generalLedgerAccountRepository;
    @InjectMocks
    private GeneralLedgerAccountService ledgerAccountService;
    private JournalEntryService journalEntryService;
    private JournalEntry journalEntry;
    private JournalEntryDetail detail;
    private JournalEntryDetail detail2;
    private TransactionDTO transactionDTO;
    private TransactionDTO transactionDTO2;
    private List<JournalEntryDetail> listDetail;
    private List<TransactionDTO> listTransaction;
    private List<JournalEntry> listJournalEntry;
    private GeneralLedgerAccount ledgerAccountAsset;
    private GeneralLedgerAccount ledgerAccountExpenses;

    @BeforeEach
    void setUp() {
        this.detail = JournalEntryDetail.builder()
                .ledgerAccount("1001")
                .credit(BigDecimal.ZERO)
                .debit(BigDecimal.TEN)
                .build();
        this.detail2 = JournalEntryDetail.builder()
                .ledgerAccount("2001")
                .credit(BigDecimal.ONE)
                .debit(BigDecimal.ZERO)
                .build();
        this.listDetail = new ArrayList<>();
        this.listDetail.add(detail);
        this.listDetail.add(detail2);
        this.journalEntry = JournalEntry.builder()
                .groupInternalId("1234")
                .memo("Pagos Test")
                .transactionDate(new Date())
                .transactionReference("12341")
                .journalEntries(this.listDetail)
                .build();
        this.transactionDTO = TransactionDTO.builder()
                .transactionDate(new Date())
                .journalId("123456789")
                .transactionReference("12345")
                .amount(BigDecimal.TEN)
                .description("Pago Test")
                .type("PAYMENTS")
                .build();
        this.transactionDTO2 = TransactionDTO.builder()
                .transactionDate(new Date())
                .journalId("987654321")
                .transactionReference("54321")
                .amount(BigDecimal.TEN)
                .description("Pago Test 2")
                .type("PAYMENTS")
                .build();
        this.listTransaction = new ArrayList<>();
        this.listTransaction.add(this.transactionDTO);
        this.listTransaction.add(this.transactionDTO2);

        this.ledgerAccountAsset = GeneralLedgerAccount.builder()
                .code("1001")
                .description("Activos")
                .currentBalance(BigDecimal.TEN)
                .openingBalance(BigDecimal.TEN)
                .name("Activos")
                .type("ASSETS")
                .build();
        this.ledgerAccountExpenses = GeneralLedgerAccount.builder()
                .code("2001")
                .description("Gastos")
                .currentBalance(BigDecimal.TEN)
                .openingBalance(BigDecimal.TEN)
                .name("Gastos")
                .type("EXPENSES")
                .build();
    }

    @DisplayName("JUnit test for getJournalEntries method")
    @Test
    void given_whenObtainJournalEntry_thenReturnJournalEntries() {
        List<JournalEntry> obtainedJournalEntries = journalEntryRepository.findAll();
        assertThat(obtainedJournalEntries).isNotNull();
    }

    @DisplayName("JUnit test for searchByTransactionReference method")
    @Test
    void givenTransactionReference_whenObtainJournalEntry_thenReturnJournalEntry() {
        try {
            JournalEntry obtainedJournalEntry = journalEntryService.searchByTransactionReference(journalEntry.getTransactionReference());
            log.info("{}",obtainedJournalEntry);
            assertThat(obtainedJournalEntry).isNotNull();
        }catch (Exception e){
            log.error("{}",e.getMessage());
        }

    }

    @DisplayName("JUnit test for getByAccountingDate method")
    @Test
    void givenPeriodtime_whenObtainListJournalEntries_thenReturnListJournalEntries() {
        try {
        List<JournalEntry> obtainedJournalEntries = journalEntryService.getByAccountingDate(new Date(),new Date());
        log.info("{}",obtainedJournalEntries.isEmpty());
        assertThat(obtainedJournalEntries).isNotNull();
        }catch (Exception e){
            log.error("{}",e.getMessage());
        }
    }

    @DisplayName("JUnit test for getReportByAccountingDate method")
    @Test
    void getReportByAccountingDate() {
    }

    @DisplayName("JUnit test for getBalanceLedgerAcount method")
    @Test
    void givenDateAndTypeAccount_whenObtainBalace_ThenReturnBalance() {
        try {
            BigDecimal obtainBalance = journalEntryService.getBalanceLedgerAcount(new Date(), LedgerAccountTypeEnum.ASSETS.getValue());
            log.info("{}",obtainBalance);
            assertThat(obtainBalance).isNotNull();
        }catch (Exception e){
            log.error("{}",e.getMessage());
        }
    }

    @DisplayName("JUnit test for createJournalEntries method")
    @Test
    void givenListTransationDTO_whenCreateJournalEntry_thenReturnJournalEntryId() {
        try {
           Assertions.assertEquals(journalEntry.getId(),journalEntryService.createJournalEntries(listTransaction));
        }catch (Exception e){
            log.error("{}",e.getMessage());
        }
    }
}