package com.banquito.accounting.service;

import com.banquito.accounting.dao.GeneralLedgerAccountRepository;
import com.banquito.accounting.enums.TransactionTypeEnum;
import com.banquito.accounting.model.GeneralLedgerAccount;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.mockito.BDDMockito.given;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.assertj.core.api.Assertions.assertThat;
import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@ExtendWith(MockitoExtension.class)
class GeneralLedgerAccountServiceTest {
    @Mock
    private GeneralLedgerAccountRepository generalLedgerAccountRepository;
    @InjectMocks
    private GeneralLedgerAccountService generalLedgerAccountService;
    private GeneralLedgerAccount generalLedgerAccount1;
    private GeneralLedgerAccount generalLedgerAccount2;
    private List<GeneralLedgerAccount> listGeneralLedgerAccount;

    @BeforeEach
    void setUp() {
        this.generalLedgerAccount1 = GeneralLedgerAccount.builder()
                .id("1")
                .type("ASSETS")
                .description("Activos")
                .name("Activos")
                .code("1001")
                .currentBalance(BigDecimal.valueOf(1000.00))
                .openingBalance(BigDecimal.valueOf(1000.00))
                .build();
        this.generalLedgerAccount2 = GeneralLedgerAccount.builder()
                .id("2")
                .type("EXPENSES")
                .description("Gastos")
                .name("Gastos")
                .code("2001")
                .currentBalance(BigDecimal.valueOf(1000.00))
                .openingBalance(BigDecimal.valueOf(1000.00))
                .build();
        this.listGeneralLedgerAccount = new ArrayList<>();
        this.listGeneralLedgerAccount.add(this.generalLedgerAccount1);
        this.listGeneralLedgerAccount.add(this.generalLedgerAccount2);
    }

    @DisplayName("JUnit test for searchByCode method")
    @Test
    void givenCode_whenObtainGeneralLedgerAccount_thenReturnGeneralLedgerAccount() {
        given(generalLedgerAccountRepository.findByCode(any())).willReturn(Optional.of(generalLedgerAccount1));
        GeneralLedgerAccount obtainedGeneralLedger = generalLedgerAccountService.searchByCode(generalLedgerAccount1.getCode());
        log.info("{}",obtainedGeneralLedger);
        assertThat(obtainedGeneralLedger).isNotNull();
    }

    @DisplayName("JUnit test for searchByType method")
    @Test
    void givenType_whenObtainGeneralLedgerAccount_thenReturnGeneralLedgerAccount() {
        given(generalLedgerAccountRepository.findByType(any())).willReturn(Optional.of(generalLedgerAccount1));
        GeneralLedgerAccount obtainedGeneralLedger = generalLedgerAccountService.searchByType(generalLedgerAccount1.getType());
        log.info("{}",obtainedGeneralLedger);
        assertThat(obtainedGeneralLedger).isNotNull();
    }

    @DisplayName("JUnit test for create method")
    @Test
    void givenGeneralAccount_whenSaveGeneralAccount_thenReturnGeneralAccount() {
        given(generalLedgerAccountRepository.save(generalLedgerAccount1)).willReturn(generalLedgerAccount1);
        GeneralLedgerAccount savedGeneralLedger = generalLedgerAccountService.create(generalLedgerAccount1);
        log.info("{}", savedGeneralLedger);
        assertThat(savedGeneralLedger).isNotNull();
    }

    @DisplayName("JUnit test for update method")
    @Test
    void givenGeneralAccount_whenUpdateGeneralAccount_thenReturnGeneralAccount() {
        given(generalLedgerAccountRepository.findByCode(any())).willReturn(Optional.of(generalLedgerAccount1));
        given(generalLedgerAccountRepository.save(any())).willReturn(generalLedgerAccount1);
        GeneralLedgerAccount savedGeneralLedger = generalLedgerAccountService.update(generalLedgerAccount1);
        log.info("{}", savedGeneralLedger);
        assertThat(savedGeneralLedger).isNotNull();
    }

    @DisplayName("JUnit test for searchByTypeForTransactions method")
    @Test
    void givenTypeTransaction_whenSearchGeneralAccount_thenExistenceGeneralAccount() {
        given(generalLedgerAccountRepository.findByType(any())).willReturn(Optional.of(generalLedgerAccount1));
        Assertions.assertEquals(true, generalLedgerAccountService.searchByTypeForTransactions(TransactionTypeEnum.PAYMENTS.getValue()));
    }

    @DisplayName("JUnit test for listGeneralLedgerAccounts method")
    @Test
    void given_whenGeneralLedger_thenReturnGeneralLedgerAccount() {
        List<GeneralLedgerAccount> obtainedListGeneralLedger = generalLedgerAccountService.listGeneralLedgerAccounts();
        log.info("{}",obtainedListGeneralLedger);
        assertThat(obtainedListGeneralLedger).isNotNull();
    }
}