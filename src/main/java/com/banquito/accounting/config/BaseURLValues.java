package com.banquito.accounting.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class BaseURLValues {
  private final String coreSettingsURL;

  public BaseURLValues(
      @Value("${banquito.core.settings.base-url}") String coreSettingsURL) {
        this.coreSettingsURL = coreSettingsURL;
  }
}
