package com.banquito.accounting.mapper;

import com.banquito.accounting.dto.GeneralLedgerAccountDTO;
import com.banquito.accounting.model.GeneralLedgerAccount;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GeneralLedgerAccountMapper {

  public static GeneralLedgerAccount buildLedgerAccount(GeneralLedgerAccountDTO dto){
    return GeneralLedgerAccount.builder()
                      .name(dto.getName())
                      .code(dto.getCode())
                      .description(dto.getDescription())
                      .type(dto.getType())
                      .currentBalance(dto.getCurrentBalance())
                      .openingBalance(dto.getOpeningBalance())
                      .build();
  }

  public static GeneralLedgerAccountDTO buildLedgerAccountDTO(GeneralLedgerAccount ledgerAccount){
    return GeneralLedgerAccountDTO.builder()
              .name(ledgerAccount.getName())
              .code(ledgerAccount.getCode())
              .description(ledgerAccount.getDescription())
              .type(ledgerAccount.getType())
              .currentBalance(ledgerAccount.getCurrentBalance())
              .openingBalance(ledgerAccount.getOpeningBalance())
              .build();
  }
}
