package com.banquito.accounting.mapper;

import com.banquito.accounting.dto.TransactionDTO;
import com.banquito.accounting.model.JournalEntry;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TransactionMapper {
  public static TransactionDTO buildTransactionDTO(JournalEntry journalEntry){
    TransactionDTO dto =
    TransactionDTO.builder()
      .transactionReference(journalEntry.getTransactionReference())
      .journalId(journalEntry.getId())
      .transactionDate(journalEntry.getTransactionDate())
      .build();   
    return dto;
    }
}
