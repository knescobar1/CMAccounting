package com.banquito.accounting.mapper;

import java.util.ArrayList;
import java.util.List;
import com.banquito.accounting.dto.JournalEntryDTO;
import com.banquito.accounting.dto.JournalEntryDetailDTO;
import com.banquito.accounting.model.JournalEntry;
import com.banquito.accounting.model.JournalEntryDetail;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JournalEntryMapper {
    
  public static JournalEntry buildJournalEntry(JournalEntryDTO dto){
    JournalEntry journalEntry =
      JournalEntry.builder()
        .transactionReference(dto.getTransactionReference())
        .accountingDate(dto.getAccountingDate())
        .transactionDate(dto.getTransactionDate())
        .memo(dto.getMemo())
        .build();
    List<JournalEntryDetail> journalEntryDetails = new ArrayList<>();
    for (JournalEntryDetailDTO journalEntryDetailDTO : dto.getJournalEntries()) {
      journalEntryDetails.add(
        JournalEntryDetail.builder()
          .ledgerAccount(journalEntryDetailDTO.getLedgerAccount())
          .debit(journalEntryDetailDTO.getDebit())
          .credit(journalEntryDetailDTO.getCredit())
          .build());
    }
    journalEntry.setJournalEntries(journalEntryDetails);
    return journalEntry;
  }

  public static JournalEntryDTO buildJournalEntryDTO(JournalEntry journalEntry){
    JournalEntryDTO dto =
      JournalEntryDTO.builder()
        .groupInternalId(journalEntry.getGroupInternalId())
        .transactionReference(journalEntry.getTransactionReference())
        .accountingDate(journalEntry.getAccountingDate())
        .transactionDate(journalEntry.getTransactionDate())
        .memo(journalEntry.getMemo())
        .build();
    List<JournalEntryDetailDTO> journalEntryDetailsDTO = new ArrayList<>();
    for (JournalEntryDetail journalEntryDetail : journalEntry.getJournalEntries()) {
      journalEntryDetailsDTO.add(
        JournalEntryDetailDTO.builder()
          .ledgerAccount(journalEntryDetail.getLedgerAccount())
          .debit(journalEntryDetail.getDebit())
          .credit(journalEntryDetail.getCredit())
          .build());
    }
    dto.setJournalEntries(journalEntryDetailsDTO);
    return dto;
  }
}
