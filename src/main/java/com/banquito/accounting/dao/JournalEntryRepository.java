package com.banquito.accounting.dao;

import com.banquito.accounting.model.JournalEntry;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface JournalEntryRepository extends MongoRepository<JournalEntry, String> {
  List<JournalEntry> findByAccountingDateBetweenOrderByAccountingDate(Date dateFrom, Date dateTo);

  List<JournalEntry> findByAccountingDateGreaterThanOrderByAccountingDate(Date dateFrom);

  List<JournalEntry> findByAccountingDateLessThanOrderByAccountingDate(Date dateTo);

  List<JournalEntry> findByJournalEntriesLedgerAccount(String generalLedgerAccount);

  List<JournalEntry> findByJournalEntriesLedgerAccountAndAccountingDateLessThanOrderByAccountingDate(String generalLedgerAccount,Date dateTo);

  List<JournalEntry> findByJournalEntriesLedgerAccountAndAccountingDateBetween(String ledgerAccount, Date dateFrom, Date dateTo);
    
  Optional<JournalEntry> findByTransactionReference(String transactionReference);
}
