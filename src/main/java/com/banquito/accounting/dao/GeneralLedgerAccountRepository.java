package com.banquito.accounting.dao;

import com.banquito.accounting.model.GeneralLedgerAccount;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.Optional;

public interface GeneralLedgerAccountRepository extends MongoRepository<GeneralLedgerAccount,String> {
  Optional<GeneralLedgerAccount> findByCode(String code);

  Optional<GeneralLedgerAccount> findByType(String type);
}
