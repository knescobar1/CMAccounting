package com.banquito.accounting.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum TransactionTypeEnum { 
  PAYMENTS("PAYMENTS", "Pagos"),
  COLLECTION("COLLECTION", "Cobros"),
  INTEREST("INTEREST", "Intereses");

  private final String value;
  private final String text;
}
