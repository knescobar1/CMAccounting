package com.banquito.accounting.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum RegionEnum {
  SIERRA("R002-EC", "Sierra");

  private final String value;
  private final String text;
}
