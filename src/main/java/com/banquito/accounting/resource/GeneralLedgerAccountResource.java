package com.banquito.accounting.resource;

import com.banquito.accounting.dto.GeneralLedgerAccountDTO;
import com.banquito.accounting.mapper.GeneralLedgerAccountMapper;
import com.banquito.accounting.model.GeneralLedgerAccount;
import com.banquito.accounting.service.GeneralLedgerAccountService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/GeneralLedgerAccount")
@RequiredArgsConstructor
public class GeneralLedgerAccountResource {
  private final GeneralLedgerAccountService ledgerAccountService;

  @ApiOperation(value="Return all G/L accounts", notes="Returns all configured G/L accounts")
  @GetMapping(path = "ledgerAccount")
  public ResponseEntity<List<GeneralLedgerAccount>> getGeneralLedgerAccount(){
    return ResponseEntity.ok(this.ledgerAccountService.listGeneralLedgerAccounts());
  }

  @ApiOperation(value="Return a G/L account by account code", notes="Return a G/L account by account code")
  @GetMapping(path = "ledgerAccount/code/{code}")
  public ResponseEntity<GeneralLedgerAccount> getLedgerAccountByCode(@ApiParam(value = "Account code", required = true) @PathVariable("code") String code){
    return ResponseEntity.ok(this.ledgerAccountService.searchByCode(code));
  }

  @ApiOperation(value="Return a G/L account by account type", notes="Return a G/L account by account type")
  @GetMapping(path = "ledgerAccount/type/{type}")
  public ResponseEntity<GeneralLedgerAccount> getLedgerAccountByType(@ApiParam(value = "Account type", required = true) @PathVariable String type){
    return ResponseEntity.ok(this.ledgerAccountService.searchByType(type));
  }

  @ApiOperation(value="Returns a validation boolean according to the type of transaction", notes="Returns a validation boolean according to the type of transaction to validate the existence of general ledger accounts")
  @GetMapping(path = "ledgerAccount/Validation/{typeTransaction}")
  public ResponseEntity<Boolean> getLedgerAccountByTypeForTransaction(@ApiParam(value = "Type of transaction (payments or collections)", required = true) @PathVariable String typeTransaction){

    return ResponseEntity.ok(this.ledgerAccountService.searchByTypeForTransactions(typeTransaction));
  }

  @ApiOperation(value="Create a General Ledger Account", notes="Create a General Ledger Account")
  @PostMapping
  public ResponseEntity<GeneralLedgerAccountDTO> create(@ApiParam(value = "General Ledger Account DTO", required = true) @RequestBody GeneralLedgerAccountDTO dto){
    try {
      GeneralLedgerAccount ledgerAccount = this.ledgerAccountService.create(GeneralLedgerAccountMapper.buildLedgerAccount(dto));
      return  ResponseEntity.ok(GeneralLedgerAccountMapper.buildLedgerAccountDTO(ledgerAccount));
    } catch (Exception e){
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @ApiOperation(value="Update a General Ledger Account", notes="Update a General Ledger Account")
  @PutMapping
  public ResponseEntity<GeneralLedgerAccountDTO> update(@ApiParam(value = "General Ledger Account DTO", required = true) @RequestBody GeneralLedgerAccountDTO dto){
    try{
      GeneralLedgerAccount ledgerAccount = this.ledgerAccountService.update(GeneralLedgerAccountMapper.buildLedgerAccount(dto));
        return ResponseEntity.ok(GeneralLedgerAccountMapper.buildLedgerAccountDTO(ledgerAccount));
      }catch (Exception e) {
        e.printStackTrace();
        return ResponseEntity.badRequest().build();
      }
  }
}
