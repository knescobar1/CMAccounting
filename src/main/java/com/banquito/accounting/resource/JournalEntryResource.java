package com.banquito.accounting.resource;

import java.util.Date;
import java.util.List;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.banquito.accounting.dto.GeneralLedgerReportDTO;
import com.banquito.accounting.dto.PeriodDTO;
import com.banquito.accounting.dto.TransactionDTO;
import com.banquito.accounting.mapper.TransactionMapper;
import com.banquito.accounting.model.JournalEntry;
import com.banquito.accounting.service.JournalEntryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(path = "/JournalEntry")
@RequiredArgsConstructor
public class JournalEntryResource {
  private final JournalEntryService service;

  @ApiOperation(value="Return all Journal Entries", notes="Return all Journal Entries")
  @GetMapping
  public ResponseEntity<List<JournalEntry>> getJournalEntries() {
    return ResponseEntity.ok(this.service.getJournalEntries());
  }

  @ApiOperation(value="Return all Journal Entries in Accounting Period for Report", notes="Return all Journal Entries in Accounting Period")
  @GetMapping(path = "/Report/{dateFrom}/{dateTo}")
  public ResponseEntity<List<GeneralLedgerReportDTO>> getGLReportInAccountingPeriod(@ApiParam(value = "Date From", required = true) @PathVariable("dateFrom") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateFrom, @ApiParam(value = "Date To", required = true) @PathVariable("dateTo") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateTo){
    return ResponseEntity.ok(this.service.getReportByAccountingDate(dateFrom, dateTo));
  }

  @ApiOperation(value="Return an Journal Entry by transaction reference", notes="Return an Journal Entry by transaction reference")
  @GetMapping(path = "/TransactionReference")
  public ResponseEntity<JournalEntry> getJournalEntryByTransactionReference(@ApiParam(value = "Transaction Reference", required = true) @PathVariable String transactionReference){
    return ResponseEntity.ok(this.service.searchByTransactionReference(transactionReference));
  }

  @ApiOperation(value="Return an Journal Entries by general ledger account", notes="Return an Journal Entries by general ledger account")
  @GetMapping(path = "/GeneralLedgerAccount/{generalLedgerAcount}")
  public ResponseEntity<List<JournalEntry>> getJournalEntryByGeneralLedgerAccount(@ApiParam(value = "General Ledger Account", required = true) @PathVariable String generalLedgerAcount){
    return ResponseEntity.ok(this.service.getByGeneralLedgerAccount(generalLedgerAcount));
  }

  @ApiOperation(value="Create an Journal Entry", notes="Create an Journal Entry from a transaction")
  @PostMapping
  public ResponseEntity<TransactionDTO> create(@ApiParam(value = "Transaction DTO", required = true) @RequestBody TransactionDTO dto) {
    try {
      JournalEntry journalEntry = this.service.create(dto.getTransactionReference(), dto.getAmount(), dto.getType(), dto.getTransactionDate(), dto.getDescription());
      return ResponseEntity.ok(TransactionMapper.buildTransactionDTO(journalEntry));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @ApiOperation(value="Create an Journal Entry", notes="Create an Journal Entry from a transaction list")
  @PostMapping(path = "/Create")
  public ResponseEntity<String> createJournalEntries(@ApiParam(value = "Transaction DTO List", required = true) @RequestBody List<TransactionDTO> dtoList) {
    try {
      String journalEntry = this.service.createJournalEntries(dtoList);
      return ResponseEntity.ok(journalEntry);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }
}
