package com.banquito.accounting.service;

import com.banquito.accounting.config.BaseURLValues;
import com.banquito.accounting.dao.GeneralLedgerAccountRepository;
import com.banquito.accounting.dao.JournalEntryRepository;
import com.banquito.accounting.dto.GeneralLedgerReportDTO;
import com.banquito.accounting.dto.HolidayDTO;
import com.banquito.accounting.dto.TransactionDTO;
import com.banquito.accounting.enums.LedgerAccountTypeEnum;
import com.banquito.accounting.enums.RegionEnum;
import com.banquito.accounting.enums.TransactionTypeEnum;
import com.banquito.accounting.exceptions.EntityNotFoundException;
import com.banquito.accounting.model.JournalEntry;
import com.banquito.accounting.model.JournalEntryDetail;
import com.banquito.accounting.model.GeneralLedgerAccount;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class JournalEntryService {
  private final JournalEntryRepository journalEntryRepository;
  private final GeneralLedgerAccountRepository generalLedgerAccountRepository;
  private final GeneralLedgerAccountService ledgerAccountService;
  private final RestTemplate restTemplate;
  private final BaseURLValues baseURLs;

  public List<JournalEntry> getJournalEntries() {
    return this.journalEntryRepository.findAll();
  }
  
  public JournalEntry searchByTransactionReference(String transactionReference) {
    Optional<JournalEntry> journalEntryOpt = this.journalEntryRepository.findByTransactionReference(transactionReference);
    if (journalEntryOpt.isEmpty()) {
        log.warn("Journal Entry does not exist for the reference transaction {}", transactionReference);
        return null;
    }
    return journalEntryOpt.get();
  }

  public List<JournalEntry> getByAccountingDate(Date dateFrom, Date dateTo) {
    return this.journalEntryRepository.findByAccountingDateBetweenOrderByAccountingDate(dateFrom, dateTo);
  }

  

  public List<GeneralLedgerReportDTO> getReportByAccountingDate(Date dateFrom, Date dateTo) {
    List<JournalEntry> listJournalEntry = new ArrayList<>();
    listJournalEntry = this.journalEntryRepository.findByAccountingDateBetweenOrderByAccountingDate(dateFrom,dateTo);
    
    List<GeneralLedgerReportDTO> generalLedgerReportDTOs = new ArrayList<>();
      BigDecimal saldo = BigDecimal.valueOf(0);
      BigDecimal saldoAssets = BigDecimal.valueOf(0);
      BigDecimal saldoExpenses = BigDecimal.valueOf(0);
      BigDecimal saldoIncomes = BigDecimal.valueOf(0);
      BigDecimal saldoInterest = BigDecimal.valueOf(0);

      for(JournalEntry journalEntry : listJournalEntry){
        List<JournalEntryDetail> listDetail = journalEntry.getJournalEntries();
        for(JournalEntryDetail detail : listDetail){
          GeneralLedgerReportDTO generalLedgerReportDTO = new GeneralLedgerReportDTO();
          generalLedgerReportDTO.setAccountingDate(journalEntry.getAccountingDate());
          generalLedgerReportDTO.setMemo(journalEntry.getMemo());
          generalLedgerReportDTO.setCredit(detail.getCredit());
          generalLedgerReportDTO.setDebit(detail.getDebit());
          generalLedgerReportDTO.setAccountCode(detail.getLedgerAccount());
          switch(detail.getLedgerAccount()){
            case "1001":
              saldoAssets = saldoAssets.add(detail.getDebit()).subtract(detail.getCredit());
              generalLedgerReportDTO.setAccountBalance(saldoAssets);
              break;
            case "2001":
              saldoExpenses = saldoExpenses.add(detail.getDebit()).subtract(detail.getCredit());
              generalLedgerReportDTO.setAccountBalance(saldoExpenses);
              break;
            case "3001":
            saldoIncomes = saldoIncomes.add(detail.getDebit()).subtract(detail.getCredit());
              generalLedgerReportDTO.setAccountBalance(saldoIncomes);
              break;
            case "4001":
            saldoInterest = saldoInterest.add(detail.getDebit()).subtract(detail.getCredit());
              generalLedgerReportDTO.setAccountBalance(saldoInterest);
              break;
            default:
              break;
          } 
          generalLedgerReportDTOs.add(generalLedgerReportDTO);
        }
        saldo = BigDecimal.valueOf(0);
      }
      generalLedgerReportDTOs.sort(Comparator.comparing(GeneralLedgerReportDTO::getAccountCode)
                                    .thenComparing(GeneralLedgerReportDTO::getCredit)
                                    .thenComparing(GeneralLedgerReportDTO::getDebit));
      
    return generalLedgerReportDTOs;
  }

  /*
  public List<GeneralLedgerReportDTO> getReportByAccountingDate(Date dateFrom, Date dateTo) {
    List<JournalEntry> listJournalEntry = new ArrayList<>();
    listJournalEntry = this.journalEntryRepository.findByAccountingDateBetweenOrderByAccountingDate(dateFrom,dateTo);
    
    List<LedgerAccountTypeEnum> listLedgerAccountType = new ArrayList<>();
    listLedgerAccountType.add(LedgerAccountTypeEnum.ASSETS);
    listLedgerAccountType.add(LedgerAccountTypeEnum.EXPENSES);
    listLedgerAccountType.add(LedgerAccountTypeEnum.INCOME);
    listLedgerAccountType.add(LedgerAccountTypeEnum.INTERESTPAYABLE);
    List<GeneralLedgerReportDTO> generalLedgerReportDTOs = new ArrayList<>();
    for (LedgerAccountTypeEnum ledgerAccountType : listLedgerAccountType){
      //BigDecimal saldo = this.getBalanceLedgerAcount(dateFrom, ledgerAccountType.getValue());
      BigDecimal saldo = BigDecimal.valueOf(0);
      for(JournalEntry journalEntry : listJournalEntry){
        List<JournalEntryDetail> listDetail = journalEntry.getJournalEntries();
        for(JournalEntryDetail detail : listDetail){
          GeneralLedgerReportDTO generalLedgerReportDTO = new GeneralLedgerReportDTO();
          generalLedgerReportDTO.setAccountingDate(journalEntry.getAccountingDate());
          generalLedgerReportDTO.setMemo(journalEntry.getMemo());
          generalLedgerReportDTO.setCredit(detail.getCredit());
          generalLedgerReportDTO.setDebit(detail.getDebit());
          Optional<GeneralLedgerAccount> generalLedgerAccount = this.generalLedgerAccountRepository.findByCode(detail.getLedgerAccount());
          GeneralLedgerAccount account = generalLedgerAccount.get();
          generalLedgerReportDTO.setAccountCode(account.getCode());
          if(ledgerAccountType.getValue().equals(account.getType())){
            saldo = saldo.add(detail.getDebit()).subtract(detail.getCredit());
            generalLedgerReportDTO.setAccountBalance(saldo);
            generalLedgerReportDTOs.add(generalLedgerReportDTO);
          }
        }
      }
    }
    return generalLedgerReportDTOs;
  }
 */

  public BigDecimal getBalanceLedgerAcount(Date dateTo, String typeAccount) {
    List<JournalEntry> listJournalEntry = this.journalEntryRepository.findByAccountingDateLessThanOrderByAccountingDate(dateTo);
    BigDecimal saldo = new BigDecimal(0);
    List<LedgerAccountTypeEnum> listLedgerAccountType = new ArrayList<>();
    listLedgerAccountType.add(LedgerAccountTypeEnum.ASSETS);
    listLedgerAccountType.add(LedgerAccountTypeEnum.EXPENSES);
    listLedgerAccountType.add(LedgerAccountTypeEnum.INCOME);

    for (LedgerAccountTypeEnum ledgerAccountType : listLedgerAccountType){
      if(ledgerAccountType.getValue().equals(typeAccount)){
        Optional<GeneralLedgerAccount> generalLedgerAccountOtp = this.generalLedgerAccountRepository.findByType(typeAccount);
        GeneralLedgerAccount generalLedgerAccount = generalLedgerAccountOtp.get();
        saldo = generalLedgerAccount.getOpeningBalance();
        for(JournalEntry journalEntry : listJournalEntry){
          Optional<GeneralLedgerAccount> generalLedgerAccountOtp2 = this.generalLedgerAccountRepository.findByCode(journalEntry.getJournalEntries().get(0).getLedgerAccount());
          GeneralLedgerAccount account = generalLedgerAccountOtp2.get();
          if(ledgerAccountType.getValue().equals(account.getType())){
            saldo = saldo.add(journalEntry.getJournalEntries().get(0).getDebit()).subtract(journalEntry.getJournalEntries().get(0).getCredit());
          }
          Optional<GeneralLedgerAccount> generalLedgerAccount2 = this.generalLedgerAccountRepository.findByCode(journalEntry.getJournalEntries().get(1).getLedgerAccount());
          GeneralLedgerAccount account2 = generalLedgerAccount2.get();
          if(ledgerAccountType.getValue().equals(account2.getType())){
            saldo = saldo.add(journalEntry.getJournalEntries().get(1).getDebit()).subtract(journalEntry.getJournalEntries().get(1).getCredit());
          }
        }
      }
    }
    return saldo;
  }

  public List<JournalEntry> getByLedgerAccountAndAccountingDate(String ledgerAccount, Date dateFrom, Date dateTo) {
    return this.journalEntryRepository.findByJournalEntriesLedgerAccountAndAccountingDateBetween(ledgerAccount,dateFrom, dateTo);
  }

  public List<JournalEntry> getByGeneralLedgerAccount(String generalLedgerAccount) {
    return this.journalEntryRepository.findByJournalEntriesLedgerAccount(generalLedgerAccount);
  }

  public JournalEntry create(String transactionReference, BigDecimal amount, String type, Date transactionDate, String description) {
    Optional<JournalEntry> journalEntryOpt = this.journalEntryRepository.findByTransactionReference(transactionReference);
    if (journalEntryOpt.isPresent()) {
        log.error("La transacción "+transactionReference+" ya se encuentra registrada en un asiento contable.");
        throw new EntityNotFoundException("No puede haber dos asientos contables con la misma transacción.");
    }
    JournalEntry journalEntry = new JournalEntry();
    journalEntry.setGroupInternalId(UUID.randomUUID().toString());
    journalEntry.setAccountingDate(new Date());
    journalEntry.setTransactionReference(transactionReference);
    journalEntry.setTransactionDate(transactionDate);
    journalEntry.setMemo(type +" "+ ((description.equals(null))?transactionReference:description));

    JournalEntryDetail journalEntryDetail1 = new JournalEntryDetail();
    JournalEntryDetail journalEntryDetail2 = new JournalEntryDetail();
    GeneralLedgerAccount ledgerAccountAsset = ledgerAccountService.searchByType(LedgerAccountTypeEnum.ASSETS.getValue());
    GeneralLedgerAccount ledgerAccountExpenses = ledgerAccountService.searchByType(LedgerAccountTypeEnum.EXPENSES.getValue());
    GeneralLedgerAccount ledgerAccountIncome = ledgerAccountService.searchByType(LedgerAccountTypeEnum.INCOME.getValue());     
    GeneralLedgerAccount ledgerAccountInterestPayment = ledgerAccountService.searchByType(LedgerAccountTypeEnum.INTERESTPAYABLE.getValue()); 
    GeneralLedgerAccount ledgerAccountAssetDB = ledgerAccountAsset;
    GeneralLedgerAccount ledgerAccountExpensesDB = ledgerAccountExpenses;
    GeneralLedgerAccount ledgerAccountIncomeDB = ledgerAccountIncome;
    GeneralLedgerAccount ledgerAccountInterestPaymentDB = ledgerAccountIncome;

    List<JournalEntryDetail> details = new ArrayList<>();
    if (type.equals(TransactionTypeEnum.PAYMENTS.getValue())) {
      journalEntryDetail1.setLedgerAccount(ledgerAccountExpenses.getCode());
      journalEntryDetail1.setCredit(BigDecimal.valueOf(0));
      journalEntryDetail1.setDebit(amount);
      journalEntryDetail2.setLedgerAccount(ledgerAccountAsset.getCode());
      journalEntryDetail2.setCredit(amount);
      journalEntryDetail2.setDebit(BigDecimal.valueOf(0));
      ledgerAccountExpensesDB.setCurrentBalance(ledgerAccountExpenses.getCurrentBalance().add(amount));
      ledgerAccountAssetDB.setCurrentBalance(ledgerAccountAsset.getCurrentBalance().subtract(amount));
      this.generalLedgerAccountRepository.save(ledgerAccountExpensesDB);
      this.generalLedgerAccountRepository.save(ledgerAccountAssetDB);
      details.add(journalEntryDetail1);
      details.add(journalEntryDetail2);
      journalEntry.setJournalEntries(details);
      this.journalEntryRepository.save(journalEntry);

      } else if (type.equals(TransactionTypeEnum.COLLECTION.getValue())) {
        journalEntryDetail1.setLedgerAccount(ledgerAccountAsset.getCode());
        journalEntryDetail1.setCredit(BigDecimal.valueOf(0));
        journalEntryDetail1.setDebit(amount);  
        journalEntryDetail2.setLedgerAccount(ledgerAccountIncome.getCode());
        journalEntryDetail2.setCredit(amount);
        journalEntryDetail2.setDebit(BigDecimal.valueOf(0));
        ledgerAccountAssetDB.setCurrentBalance(ledgerAccountAsset.getCurrentBalance().add(amount));
        ledgerAccountIncomeDB.setCurrentBalance(ledgerAccountIncome.getCurrentBalance().subtract(amount));
        this.generalLedgerAccountRepository.save(ledgerAccountAssetDB);
        this.generalLedgerAccountRepository.save(ledgerAccountIncomeDB);
        details.add(journalEntryDetail1);
        details.add(journalEntryDetail2);
        journalEntry.setJournalEntries(details);
        this.journalEntryRepository.save(journalEntry);
      }else if (type.equals(TransactionTypeEnum.INTEREST.getValue())) {
        journalEntryDetail1.setLedgerAccount(ledgerAccountInterestPayment.getCode());
        journalEntryDetail1.setCredit(BigDecimal.valueOf(0));
        journalEntryDetail1.setDebit(amount);  
        journalEntryDetail2.setLedgerAccount(ledgerAccountAsset.getCode());
        journalEntryDetail2.setCredit(amount);
        journalEntryDetail2.setDebit(BigDecimal.valueOf(0));
        ledgerAccountInterestPaymentDB.setCurrentBalance(ledgerAccountInterestPayment.getCurrentBalance().add(amount));
        ledgerAccountAssetDB.setCurrentBalance(ledgerAccountAsset.getCurrentBalance().subtract(amount));
        this.generalLedgerAccountRepository.save(ledgerAccountInterestPaymentDB);
        this.generalLedgerAccountRepository.save(ledgerAccountAssetDB);
        details.add(journalEntryDetail1);
        details.add(journalEntryDetail2);
        journalEntry.setJournalEntries(details);
        this.journalEntryRepository.save(journalEntry);
      }
      log.info("Journal Entry created: {}, {}, {}", journalEntry.getId(),journalEntry.getTransactionReference(),journalEntry.getAccountingDate());
      return journalEntry;
  }

  public String createJournalEntries(List<TransactionDTO> dtoList) {
    Boolean condition = true; 
    String referenceTransaction = "";
    String code = "";
    String type = "";
    BigDecimal amountTotal = new BigDecimal(0);
    JournalEntry journalEntry = new JournalEntry();
    journalEntry.setGroupInternalId(UUID.randomUUID().toString());

    List<JournalEntryDetail> details = new ArrayList<>();

    GeneralLedgerAccount ledgerAccountAsset = ledgerAccountService.searchByType(LedgerAccountTypeEnum.ASSETS.getValue());
    GeneralLedgerAccount ledgerAccountExpenses = ledgerAccountService.searchByType(LedgerAccountTypeEnum.EXPENSES.getValue());
    GeneralLedgerAccount ledgerAccountIncome = ledgerAccountService.searchByType(LedgerAccountTypeEnum.INCOME.getValue());     
    GeneralLedgerAccount ledgerAccountInterestPayment = ledgerAccountService.searchByType(LedgerAccountTypeEnum.INTERESTPAYABLE.getValue()); 
    GeneralLedgerAccount ledgerAccountAssetDB = ledgerAccountAsset;
    GeneralLedgerAccount ledgerAccountExpensesDB = ledgerAccountExpenses;
    GeneralLedgerAccount ledgerAccountIncomeDB = ledgerAccountIncome;
    GeneralLedgerAccount ledgerAccountInterestPaymentDB = ledgerAccountIncome;

    JournalEntryDetail journalEntryDetail1 = new JournalEntryDetail();

    for(TransactionDTO dto : dtoList)
    {
      Optional<JournalEntry> journalEntryOpt = this.journalEntryRepository.findByTransactionReference(dto.getTransactionReference());
      if (journalEntryOpt.isPresent()) {
          log.error("La transacción "+dto.getTransactionReference()+" ya se encuentra registrada en un asiento contable.");
          throw new EntityNotFoundException("No puede haber dos asientos contables con la misma transacción.");
      }
      referenceTransaction = referenceTransaction + " " + dto.getTransactionReference();
      if(condition)
      {
        Boolean existHoliday = false;
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        try {
          String fechaActual = simpleDateFormat.format(new Date());
          final Map<String, String> variables = new HashMap<>();
          variables.put("startdate", fechaActual);
          variables.put("finalDate", fechaActual);
          variables.put("region", RegionEnum.SIERRA.getValue());
          ResponseEntity<HolidayDTO[]> response = this.restTemplate.getForEntity(baseURLs.getCoreSettingsURL() + "/holiday/search/beetweenDatesAndRegion/{startdate}/{finalDate}/{region}", HolidayDTO[].class,variables);
          HolidayDTO[] objectArray = response.getBody();
          List<HolidayDTO> listHolidays = Arrays.asList(objectArray);
          for(HolidayDTO holidayDTO : listHolidays){
            log.info("Fechas: {}", holidayDTO.getDate().toString());
            if(simpleDateFormat.format(holidayDTO.getDate()).equals(fechaActual) )
            {
              existHoliday = true;
              break;
            }
          }
        } catch (Exception e) {
          log.error("{}", e.getMessage());
        }
        Date fechaContabiliazcion = dto.getTransactionDate();
        if(existHoliday)
        {
          Calendar c = Calendar.getInstance();
          c.setTime(dto.getTransactionDate());
          c.add(Calendar.DATE, 1);
          fechaContabiliazcion = c.getTime();
        }

        journalEntry.setAccountingDate(fechaContabiliazcion);
        journalEntry.setTransactionDate(dto.getTransactionDate());
        journalEntry.setMemo(dto.getType() +" "+ dto.getDescription());
        
        if(dto.getType().equals(TransactionTypeEnum.PAYMENTS.getValue())){
          code = ledgerAccountExpenses.getCode();
          type = dto.getType();
        }else if(dto.getType().equals(TransactionTypeEnum.COLLECTION.getValue())){
          code = ledgerAccountAsset.getCode();
          type = dto.getType();
        }else if(dto.getType().equals(TransactionTypeEnum.INTEREST.getValue())){
          code = ledgerAccountInterestPayment.getCode();
          type = dto.getType();
        }
        condition=false;
      }
      
      JournalEntryDetail journalEntryDetail2 = new JournalEntryDetail();
     

      amountTotal = amountTotal.add(dto.getAmount());
      if (dto.getType().equals(TransactionTypeEnum.PAYMENTS.getValue())) {
       
        journalEntryDetail2.setLedgerAccount(ledgerAccountAsset.getCode());
        journalEntryDetail2.setCredit(dto.getAmount());
        journalEntryDetail2.setDebit(BigDecimal.valueOf(0));        
        ledgerAccountAssetDB.setCurrentBalance(ledgerAccountAsset.getCurrentBalance().subtract(dto.getAmount()));
        this.generalLedgerAccountRepository.save(ledgerAccountAssetDB);
        details.add(journalEntryDetail2);
        } else if (dto.getType().equals(TransactionTypeEnum.COLLECTION.getValue())) { 
          journalEntryDetail2.setLedgerAccount(ledgerAccountIncome.getCode());
          journalEntryDetail2.setCredit(dto.getAmount());
          journalEntryDetail2.setDebit(BigDecimal.valueOf(0));
          ledgerAccountIncomeDB.setCurrentBalance(ledgerAccountIncome.getCurrentBalance().subtract(dto.getAmount()));
          this.generalLedgerAccountRepository.save(ledgerAccountIncomeDB);
          details.add(journalEntryDetail2);
        }else if (dto.getType().equals(TransactionTypeEnum.INTEREST.getValue())) {
          journalEntryDetail2.setLedgerAccount(ledgerAccountAsset.getCode());
          journalEntryDetail2.setCredit(dto.getAmount());
          journalEntryDetail2.setDebit(BigDecimal.valueOf(0));
          ledgerAccountAssetDB.setCurrentBalance(ledgerAccountAsset.getCurrentBalance().subtract(dto.getAmount()));
          this.generalLedgerAccountRepository.save(ledgerAccountAssetDB);
          details.add(journalEntryDetail2);
        } 
    }

    if(type.equals(TransactionTypeEnum.PAYMENTS.getValue())){
      ledgerAccountExpensesDB.setCurrentBalance(ledgerAccountExpenses.getCurrentBalance().add(amountTotal));
      this.generalLedgerAccountRepository.save(ledgerAccountExpensesDB);
    }else if(type.equals(TransactionTypeEnum.COLLECTION.getValue())){
      ledgerAccountAssetDB.setCurrentBalance(ledgerAccountAsset.getCurrentBalance().add(amountTotal));
      this.generalLedgerAccountRepository.save(ledgerAccountAssetDB);
    }else if(type.equals(TransactionTypeEnum.INTEREST.getValue())){
      ledgerAccountInterestPaymentDB.setCurrentBalance(ledgerAccountInterestPayment.getCurrentBalance().add(amountTotal));
      this.generalLedgerAccountRepository.save(ledgerAccountInterestPaymentDB);
    }

    journalEntry.setTransactionReference(referenceTransaction);
    journalEntryDetail1.setLedgerAccount(code);
    journalEntryDetail1.setCredit(BigDecimal.valueOf(0));
    journalEntryDetail1.setDebit(amountTotal);
    details.add(journalEntryDetail1);
    journalEntry.setJournalEntries(details);
    this.journalEntryRepository.save(journalEntry);
    log.info("Journal Entry created: {}, {}, {}", journalEntry.getId(),journalEntry.getTransactionReference(),journalEntry.getAccountingDate());
    return journalEntry.getId();
  }
}