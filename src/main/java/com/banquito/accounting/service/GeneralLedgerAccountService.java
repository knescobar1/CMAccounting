package com.banquito.accounting.service;

import com.banquito.accounting.dao.GeneralLedgerAccountRepository;
import com.banquito.accounting.enums.LedgerAccountTypeEnum;
import com.banquito.accounting.enums.TransactionTypeEnum;
import com.banquito.accounting.exceptions.EntityNotFoundException;
import com.banquito.accounting.model.GeneralLedgerAccount;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class GeneralLedgerAccountService {
  private final GeneralLedgerAccountRepository generalLedgerAccountRepository;

  public GeneralLedgerAccount searchById(String id) {
    Optional<GeneralLedgerAccount> genelalLedgerAccountOpt = this.generalLedgerAccountRepository.findById(id);
    if (genelalLedgerAccountOpt.isEmpty()) {
      log.warn("A G/L account is not configured with the id {}" + id);

      return null;
    }
    return genelalLedgerAccountOpt.get();
  }

  public List<GeneralLedgerAccount> listGeneralLedgerAccounts(){
    return this.generalLedgerAccountRepository.findAll();
  }
    
  public GeneralLedgerAccount searchByCode(String code) {
    Optional<GeneralLedgerAccount> genelalLedgerAccountOpt = this.generalLedgerAccountRepository.findByCode(code);
    if (genelalLedgerAccountOpt.isEmpty()) {
      log.warn("A G/L account is not configured with the code {}" + code);
      return null;
    }
    return genelalLedgerAccountOpt.get();
  }

  public GeneralLedgerAccount searchByType(String type){
    Optional<GeneralLedgerAccount> genelalLedgerAccountOpt = this.generalLedgerAccountRepository.findByType(type);
    if (genelalLedgerAccountOpt.isEmpty()) {
      log.warn("A G/L account is not configured with the type {}" + type);
      return null;
    }
    return genelalLedgerAccountOpt.get();
  }

  public Boolean searchByTypeForTransactions(String typeTransaction){
    Optional<GeneralLedgerAccount> genelalLedgerAccountOpt1 = this.generalLedgerAccountRepository.findByType(LedgerAccountTypeEnum.ASSETS.getValue());
    if(typeTransaction.equals(TransactionTypeEnum.PAYMENTS.getValue()))
    {
      Optional<GeneralLedgerAccount> genelalLedgerAccountOpt2 = this.generalLedgerAccountRepository.findByType(LedgerAccountTypeEnum.EXPENSES.getValue());
      if(genelalLedgerAccountOpt1.isEmpty() || genelalLedgerAccountOpt2.isEmpty()){
        log.warn("A G/L account is not configured for the type of transaction {}" + typeTransaction);
        return false;
      }else{
        return true;
      }
    }else{
      Optional<GeneralLedgerAccount> genelalLedgerAccountOpt2 = this.generalLedgerAccountRepository.findByType(LedgerAccountTypeEnum.INCOME.getValue());
      if(genelalLedgerAccountOpt1.isEmpty() || genelalLedgerAccountOpt2.isEmpty()){
        log.warn("A G/L account is not configured for the type of transaction {}" + typeTransaction);
        return false;
      }else{
        return true;
      }
    }
  }
  
  public GeneralLedgerAccount create(GeneralLedgerAccount ledgerAccount) {
    Optional<GeneralLedgerAccount> generalLedgerAccountDB = this.generalLedgerAccountRepository.findByCode(ledgerAccount.getCode());
    if (generalLedgerAccountDB.isPresent()) {
      log.error("A general ledger account already exists with the code {}" + ledgerAccount.getCode());
      throw new EntityNotFoundException("There is already a general ledger account with this code.");
    }
    return this.generalLedgerAccountRepository.save(ledgerAccount);
  }

    public GeneralLedgerAccount update(GeneralLedgerAccount ledgerAccount) {
      Optional<GeneralLedgerAccount> generalLedgerAccountOpt = this.generalLedgerAccountRepository.findByCode(ledgerAccount.getCode());
      if (generalLedgerAccountOpt.isEmpty()) {
        log.error("Ledger account not found with code {}"+ledgerAccount.getCode());
        throw new EntityNotFoundException("Ledger account not found with this code");
      }
      GeneralLedgerAccount generalLedgerAccountDB = generalLedgerAccountOpt.get();
      generalLedgerAccountDB.setName(ledgerAccount.getName());
      generalLedgerAccountDB.setType(generalLedgerAccountDB.getType());
      generalLedgerAccountDB.setCurrentBalance(ledgerAccount.getCurrentBalance());
      log.info("Ledger Account update: {} , {} , {}", generalLedgerAccountDB.getCode(),generalLedgerAccountDB.getName(), generalLedgerAccountDB.getType());
      return this.generalLedgerAccountRepository.save(generalLedgerAccountDB);
    }
}
