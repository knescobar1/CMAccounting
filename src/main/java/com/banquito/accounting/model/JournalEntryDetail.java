package com.banquito.accounting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JournalEntryDetail {

  private String ledgerAccount;

  private BigDecimal debit;

  private BigDecimal credit;
}
