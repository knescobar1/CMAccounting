package com.banquito.accounting.dto;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PeriodDTO {
    
    private Date dateFrom;

    private Date dateTo;
}
