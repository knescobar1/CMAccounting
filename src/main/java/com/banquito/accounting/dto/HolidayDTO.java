package com.banquito.accounting.dto;

import java.util.Date;

import lombok.Data;

@Data
public class HolidayDTO {

    private Date date;

    private String name;
    
    private String regionId;
}
