package com.banquito.accounting.dto;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GeneralLedgerAccountDTO {

    private String name;

    private String type;

    private String description; 

    private String code;

    private BigDecimal currentBalance;

    private BigDecimal openingBalance;
}
