package com.banquito.accounting.dto;

import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GeneralLedgerReportDTO {
  private String accountCode;

  private Date accountingDate;

  private String memo;

  private BigDecimal debit;

  private BigDecimal credit;

  private BigDecimal accountBalance;
}
